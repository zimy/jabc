plugins {
    java
    jacoco
    application
}

group = "solutions.untested.hometools.duabc"
java.sourceCompatibility = JavaVersion.VERSION_16

application {
    mainClass.set("solutions.untested.hometools.ab.DuABCJ")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("commons-io:commons-io:2.9.0")
}

tasks.withType<Test> {
    useJUnitPlatform()
}
