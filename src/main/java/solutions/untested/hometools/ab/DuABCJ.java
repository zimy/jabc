package solutions.untested.hometools.ab;

import org.apache.commons.io.FileUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.getProperty;
import static java.lang.System.out;
import static java.nio.file.Files.list;
import static java.nio.file.Paths.get;
import static java.util.Comparator.comparingLong;
import static java.util.stream.Collectors.joining;

public class DuABCJ {

    enum ABC {
        A, B, C
    }

    record NameAndSize(String fileName, long size) {
    }

    record NameSizeAndPercentOfTotal(String fileName, long size, float percentOfTotal) {
    }

    record NameSizePercentsAndLetter(String fileName, long size, float percentOfTotal, float cumulativePercents,
                                     ABC abc) {
    }

    public static void main(String[] args) throws IOException {
        String property = getProperty("user.dir");
        Path path = get(property);

        List<NameAndSize> nameAndSizes = list(path)
                .map(path1 -> new NameAndSize(path1.getFileName().toString(), FileUtils.sizeOf(path1.toFile())))
                .sorted(comparingLong(o -> o.size)).toList();

        if (nameAndSizes.size() < 2) {
            return;
        }

        long total = nameAndSizes.stream().mapToLong(value -> value.size).sum();
        if (total == 0) {
            return;
        }

        List<NameSizeAndPercentOfTotal> nameSizeAndPercentOfTotals = nameAndSizes.stream()
                .map(nameAndSize -> new NameSizeAndPercentOfTotal(nameAndSize.fileName, nameAndSize.size, nameAndSize.size / (float) total))
                .toList();


        float accumulator = 0;
        List<NameSizePercentsAndLetter> result = new ArrayList<>();
        for (int i = 0; i < nameSizeAndPercentOfTotals.size(); i++) {
            NameSizeAndPercentOfTotal nameSizeAndPercentOfTotal = nameSizeAndPercentOfTotals.get(i);
            if (i == 0) {
                result.add(new NameSizePercentsAndLetter(nameSizeAndPercentOfTotal.fileName, nameSizeAndPercentOfTotal.size, nameSizeAndPercentOfTotal.percentOfTotal, nameSizeAndPercentOfTotal.percentOfTotal, ABC.C));
                accumulator = nameSizeAndPercentOfTotal.percentOfTotal;
            }
            accumulator += nameSizeAndPercentOfTotal.percentOfTotal;
            result.add(new NameSizePercentsAndLetter(nameSizeAndPercentOfTotal.fileName, nameSizeAndPercentOfTotal.size, nameSizeAndPercentOfTotal.percentOfTotal, accumulator, accumulator < 0.05 ?
                    ABC.C : (accumulator < 0.2 ? ABC.B : ABC.A)));
        }
        String collect1 = result.stream()
                .map(nameAndSize -> nameAndSize.fileName + ", " + nameAndSize.percentOfTotal + ", " + nameAndSize.cumulativePercents + ", " + nameAndSize.abc)
                .collect(joining("\n"));
        out.println(collect1);
    }
}
